package com.example.demo.exception;

import org.springframework.http.HttpStatus;

public class ResourceNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResourceNotFound() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public ResourceNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}


	public ResourceNotFound(HttpStatus code, String message) {
		super(code, message);
		// TODO Auto-generated constructor stub
	}

	
}
