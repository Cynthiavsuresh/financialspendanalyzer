package com.example.demo.dto;

public enum PaymentType {
	
	CREDIT,
	DEBIT

}
