package com.example.demo.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.example.demo.entity.Customerdetails;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Transactiondto {
	
	
	private Customerdetails customer;
	@NotBlank(message = "describtion is required")
	private String describtion;	
	@Enumerated(EnumType.STRING)
	private Category category;
	@Min(value = 1, message="minimum amount should be atleast Rs.1")
	private BigDecimal amount;
	@Enumerated(EnumType.STRING)
	private PaymentType paymenttype;
	private LocalDateTime paymenttime;

}
