package com.example.demo.dto;

public enum Category {
	House,
	Groceries,
	Self,
	Business,
	Travel

}
