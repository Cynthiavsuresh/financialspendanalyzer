package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Transactiondetails;

public interface Transactionrepository extends JpaRepository<Transactiondetails, String> {

}
