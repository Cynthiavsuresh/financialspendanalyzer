package com.example.demo.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Customerdetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String customerId;
	private String name;
	private String Accountno;
	private String mobile;
	private String mail;
	private int age;
	private String password;
	private BigDecimal currentbalance;
	
	

}
