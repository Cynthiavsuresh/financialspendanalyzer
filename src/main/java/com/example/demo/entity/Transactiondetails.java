package com.example.demo.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.example.demo.dto.Category;
import com.example.demo.dto.PaymentType;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Transactiondetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String transactionId;
	@ManyToOne
	@JoinColumn(name="customerId")
	private Customerdetails customer;
	private String describtion;	
	@Enumerated(EnumType.STRING)
	private Category category;
	private BigDecimal amount;
	@Enumerated(EnumType.STRING)
	private PaymentType paymenttype;
	private LocalDateTime paymenttime;
	
	
	
	
	

}
